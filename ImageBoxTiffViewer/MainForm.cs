﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
// Displaying multi-page tiff files using the ImageBox control and C#

namespace Demo.TiffViewer
{
  public partial class MainForm : Form
  {
    #region Fields

    private int _currentPage;

    private Image _openImage;

    private int _pageCount;

    #endregion

    #region Constructors

    public MainForm()
    {
      this.InitializeComponent();
    }

    #endregion

    #region Methods

    /// <summary>Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event.</summary>
    /// <param name="e">A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains the event data. </param>
    protected override void OnFormClosing(FormClosingEventArgs e)
    {
      base.OnFormClosing(e);

      if (!e.Cancel)
      {
        this.CloseImage();
      }
    }

    /// <summary>Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event.</summary>
    /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    protected override void OnShown(EventArgs e)
    {
      string[] arguments;

      base.OnShown(e);

      arguments = Environment.GetCommandLineArgs();

      this.OpenImage(arguments.Length > 1 ? arguments[1] : "NewtonsCradle.tif");
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      using (AboutDialog dialog = new AboutDialog())
      {
        dialog.ShowDialog(this);
      }
    }

    private void CloseImage()
    {
      if (_openImage != null)
      {
        imageBox.Image = null;
        _openImage.Dispose();
        _openImage = null;
      }
    }

    private void copyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Clipboard.SetImage(imageBox.Image);
      }
      catch (ExternalException ex)
      {
        MessageBox.Show($"Failed to copy image to the Clipboard. {ex.GetBaseException().Message}", "Copy Image", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void firstPageToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.ShowPage(1);
    }

    private int GetPageCount(Image image)
    {
      FrameDimension dimension;

      dimension = FrameDimension.Page;

      return image.GetFrameCount(dimension);
    }

    private void lastPageToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.ShowPage(_pageCount);
    }

    private void nextPageToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.ShowPage(_currentPage + 1);
    }

    private void OpenImage(string fileName)
    {
      fileName = Path.Combine(Environment.CurrentDirectory, fileName);

      if (!File.Exists(fileName))
      {
        MessageBox.Show($"Cannot find file '{fileName}'", "Open File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
      else
      {
        try
        {
          this.OpenImage(Image.FromFile(fileName));
        }
        catch (OutOfMemoryException ex)
        {
          MessageBox.Show($"Failed to open file '{fileName}'. {ex.GetBaseException().Message}", "Open File", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
    }

    private void OpenImage(Image image)
    {
      this.CloseImage();

      _openImage = image;
      _pageCount = this.GetPageCount(image);

      imageBox.Image = _openImage;

      this.ShowPage(1);

    //  imageBox.ZoomToFit();
    }

    private void OpenImage()
    {
      using (OpenFileDialog dialog = new OpenFileDialog
                                     {
                                       Title = "Open Image",
                                       DefaultExt = "tif",
                                       Filter = "All Pictures(*.emf; *.wmf; *.jpg; *.jpeg; *.jfif; *.jpe; *.png; *.bmp; *.dib; *.rle; *.gif; *.tif; *.tiff)| *.emf; *.wmf; *.jpg; *.jpeg; *.jfif; *.jpe; *.png; *.bmp; *.dib; *.rle; *.gif; *.tif; *.tiff | Windows Enhanced Metafile (*.emf) | *.emf | Windows Metafile(*.wmf) | *.wmf | JPEG File Interchange Format(*.jpg; *.jpeg; *.jfif; *.jpe)| *.jpg; *.jpeg; *.jfif; *.jpe | Portable Networks Graphic (*.png) | *.png | Windows Bitmap(*.bmp; *.dib; *.rle)| *.bmp; *.dib; *.rle | Graphics Interchange Format (*.gif) | *.gif | Tagged Image File Format(*.tif; *.tiff)| *.tif; *.tiff | All files(*.*) | *.*"
                                     })
      {
        if (dialog.ShowDialog(this) == DialogResult.OK)
        {
          this.OpenImage(dialog.FileName);
        }
      }
    }

    private void openToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.OpenImage();
    }

    private void previousPageToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.ShowPage(_currentPage - 1);
    }

    private void SaveImageFrame()
    {
      using (SaveFileDialog dialog = new SaveFileDialog
                                     {
                                       Filter = "Portable Networks Graphic (*.png)|*.png|All Files (*.*)|*.*",
                                       DefaultExt = "png",
                                       Title = "Save Frame As"
                                     })
      {
        if (dialog.ShowDialog(this) == DialogResult.OK)
        {
          this.SaveImageFrame(dialog.FileName);
        }
      }
    }

    private void SaveImageFrame(string fileName)
    {
      try
      {
        _openImage.Save(fileName, ImageFormat.Png);
      }
      catch (ExternalException ex)
      {
        MessageBox.Show($"Failed to save file '{fileName}'. {ex.GetBaseException().Message}", "Save File", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void saveToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.SaveImageFrame();
    }

    private void ShowPage(int page)
    {
      _currentPage = page;

      // update the current frame of the image
      _openImage.SelectActiveFrame(FrameDimension.Page, page - 1);

      // The ImageBox control doesn't know the image 
      // has changed, so force a repaint
     imageBox.Invalidate();

      this.UpdateUi();
    }

    private void UpdateUi()
    {
      bool canMovePrevious;
      bool canMoveNext;

      pageToolStripStatusLabel.Text = $"{_currentPage} of {_pageCount}";

      canMovePrevious = _currentPage > 1;
      canMoveNext = _currentPage < _pageCount;

      firstPageToolStripButton.Enabled = canMovePrevious;
      firstPageToolStripMenuItem.Enabled = canMovePrevious;

      previousPageToolStripButton.Enabled = canMovePrevious;
      previousPageToolStripMenuItem.Enabled = canMovePrevious;

      nextPageToolStripButton.Enabled = canMoveNext;
      nextPageToolStripMenuItem.Enabled = canMoveNext;

      lastPageToolStripButton.Enabled = canMoveNext;
      lastPageToolStripMenuItem.Enabled = canMoveNext;
    }

        #endregion

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            StitchTiffPages();
        }

        private Image StitchTiffPages()
        {

            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            //Get an ImageCodecInfo object that represents the TIFF codec.
            myImageCodecInfo = GetEncoderInfo("image/tiff");

            //Create an Encoder object based on the GUID for the SaveFlag parameter category.
            myEncoder = Encoder.SaveFlag;
            //Create an EncoderParameters object.
            //An EncoderParameters object has an array of EncoderParameter objects. In this case, there is only one
            //EncoderParameter object in the array.

            myEncoderParameters = new EncoderParameters(1);
            myEncoderParameter = new EncoderParameter(myEncoder, (long)EncoderValue.MultiFrame);
            myEncoderParameters.Param[0] = myEncoderParameter;
            string destFile = "C:\\AAA\\ParagonManRedact\\ParagonManRedact\\bin\\Debug\\Working\\01917001-s.tif";
            string FileFullName = "C:\\AAA\\ParagonManRedact\\ParagonManRedact\\bin\\Debug\\Working\\01917001.tif";
            string CurrentPageLocation = "C:\\AAA\\ParagonManRedact\\ParagonManRedact\\bin\\Debug\\Working\\01917001";

        //   // if(File.Exists(Cu))
        //    System.Drawing.Image DestinationImage = (System.Drawing.Image)(new System.Drawing.Bitmap((string)sourceFiles[0]));

        //    DestinationImage.Save(str_DestinationPath, codec, imagePararms);

        //    imagePararms.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)System.Drawing.Imaging.EncoderValue.FrameDimensionPage);


        //    for (int i = 0; i < sourceFiles.Length - 1; i++)
        //    {
        //        System.Drawing.Image img = (System.Drawing.Image)(new System.Drawing.Bitmap((string)sourceFiles[i]));

        //        DestinationImage.SaveAdd(img, imagePararms);
        //        img.Dispose();
        //    }

        //    imagePararms.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)System.Drawing.Imaging.EncoderValue.Flush);
        //    DestinationImage.SaveAdd(imagePararms);
        //    imagePararms.Dispose();
        //    DestinationImage.Dispose();

        //}






        
            string PageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page0.tif";

            if(File.Exists(PageLocation))
            {
                 System.Drawing.Image DestinationImage = (System.Drawing.Image)(new System.Drawing.Bitmap(PageLocation));

              DestinationImage.Save(destFile, myImageCodecInfo, myEncoderParameters);



                try
                {
                    for (int i = 1; i < _pageCount; i++)
                    {
                        PageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page" + i + ".tif";

                        myEncoderParameter = new EncoderParameter(myEncoder, (long)EncoderValue.FrameDimensionPage);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        if (File.Exists(PageLocation))
                        {
                            
                           
                            System.Drawing.Image img = (System.Drawing.Image)(new System.Drawing.Bitmap(PageLocation));

                                    DestinationImage.SaveAdd(img, myEncoderParameters);
                                   img.Dispose();
                          
                        }
                    }
                    myEncoderParameters.Param[0] = new EncoderParameter(myEncoder, (long)EncoderValue.Flush);
                 //   saveTif.SaveAdd(myEncoderParameters);

                    myEncoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)System.Drawing.Imaging.EncoderValue.Flush);
                    DestinationImage.SaveAdd(myEncoderParameters);
                    myEncoderParameters.Dispose();
                    DestinationImage.Dispose();
                    return DestinationImage;

                }
                catch (Exception)
                {
                   // logger.Log(LogLevel.Error, e.InnerException);
                }
            }

            return null;
        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {

            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (int i = 0; i < encoders.Length; i++)
            {
                if (encoders[i].MimeType == mimeType)
                    return encoders[i];

            }
            return null;
        }
    }
}

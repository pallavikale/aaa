﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using Rectangle = System.Drawing.Rectangle;
using NLog;
using System.Data;
using System.Globalization;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace ParagonManRedact
{
    public struct gRectangle
    {
        public int sRedActx;
        public int eRedActx;
        public int sRedActy;
        public int eRedcty;

        public gRectangle(int sx, int ex, int sy, int ey)
        {
            sRedActx = sx;
            eRedActx = ex;
            sRedActy = sy;
            eRedcty = ey;
        }
    }

    public partial class Main : Form
    {
        bool Paint;
        bool IsLoading = false;
        SolidBrush color;
        string FileFullName;
        string Filename;
        string ImageID = string.Empty;
        string ImageVersion = string.Empty;
        string FileExt;
        string OutPath;
        string WorkingPath;
        Bitmap sImage;
        Image mImage;
        int sTartX;
        int sTartY;
        int FileCounter = 0;
        string thistext;
        List<gRectangle> rects = new List<gRectangle>();
        List<FileDetail> FileList = new List<FileDetail>();
        private const string configDirectoryPath = "sourceDirectoryPath";
        private const string dailyDirectoryPath = "dailyDirectoryPath";
        private const string historicDirectoryPath = "historicDirectoryPath";

        int vScrollValue;
        int hScrollValue;
        private DateTime dateTimeBeforeOpeningFile;
        private SqlConnection dbConnection = null;
        private bool isImageChanged = false;
        private FileDetail currentFileDetails = null;
        private bool isRedactionDone = false;
        private bool ArchiveFiles = true;
        private bool inTestingPhase;
        private static Logger logger = LogManager.GetLogger("ManualUI");
        string pdfReaderName = string.Empty;
        bool OpenPdfFilesToRedact = false;
        string selectedDirectoryLocation = string.Empty;
        bool isDailySelected = false;
        int previousScrollValue = 0;
        int selectedFolderCount = 0;
        int TotalFilesProcessed = 0;

        // variables added for pagination
        private int _currentPage;
        private Image _openImage;
        private int _pageCount;
        private string CurrentPageLocation;
        private string ArchiveFolderPath = string.Empty;
        private string DailyTableName = string.Empty;
        private string HistoricTableName = string.Empty;


        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {


            string dailyDirectoryLocation = ReadAppSettings(dailyDirectoryPath);
            string historicDirectoryLocation = ReadAppSettings(historicDirectoryPath);
            WorkingFolderSelection workingFolderSelection = new WorkingFolderSelection(dailyDirectoryLocation, historicDirectoryLocation);
            bool.TryParse(ReadAppSettings("ArchiveFiles"), out ArchiveFiles);
            workingFolderSelection.ShowDialog();
            // uncommented the file count code 
            // workingFolderSelection.DisplayFileCountDetails();

            if (workingFolderSelection.quitApplication == true)
            {
                this.Close();

            }

            else
            {

                selectedDirectoryLocation = workingFolderSelection.selectedFolder;

                isDailySelected = workingFolderSelection.isDailySelected;

                selectedFolderCount = isDailySelected ? workingFolderSelection.dailyFolderCount : workingFolderSelection.historicFolderCount;

                logger.Log(LogLevel.Debug, "Program initialized");

                inTestingPhase = Convert.ToBoolean(ReadAppSettings("InTestingPhase"));

                DailyTableName = ReadAppSettings("DailyTableName");
                HistoricTableName = ReadAppSettings("HistoricTableName");

                if(!inTestingPhase && string.IsNullOrEmpty(DailyTableName))
                {
                    logger.Log(LogLevel.Error, "Please provide DailyTableName in the configuration file for database updation");
                }

                if (!inTestingPhase && string.IsNullOrEmpty(HistoricTableName))
                {
                    logger.Log(LogLevel.Error, "Please provide HistoricTableName in the configuration file for database updation");
                }

                IsLoading = true;
                //   WorkingPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Working");

                logger.Log(LogLevel.Debug, "Mapping remote drive");
                // MapDrive();
              //  pdfReaderName = ReadAppSettings("PDFReader");
                OpenPdfFilesToRedact = Convert.ToBoolean(ReadAppSettings("OpenPdfFile"));
                //ConnectToRemoteMachine();
                ArchiveFolderPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Archive");
                if (!Directory.Exists(ArchiveFolderPath))
                {
                    logger.Log(LogLevel.Debug, "Creating archive directory " + ArchiveFolderPath);
                    Directory.CreateDirectory(ArchiveFolderPath);

                }

                WorkingPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Working");
                logger.Log(LogLevel.Info, "Working Folder location: " + WorkingPath);
                if (!Directory.Exists(WorkingPath))
                {
                    logger.Log(LogLevel.Debug, "Creating working directory " + WorkingPath);
                    Directory.CreateDirectory(WorkingPath);

                }
                else
                {

                    MoveFilesFromWorkingFolderToSourceFolder();
                }

                thistext = this.Text;
                if (!inTestingPhase)
                {
                    Connect();
                }

                //  GetFileList(selectedFolderCount);
                rects.Clear();
                btnRotate.Enabled = true;
                rotateToolStripMenuItem.Enabled = true;
                btnPrev.Enabled = false;
                previousToolStripMenuItem.Enabled = false;
                logger.Log(LogLevel.Info, " starting to load image");
                OpenNextImage(0);
                UpdateFileCounter();

            }
        }



        private void MapDrive()
        {
            string remoteMachineName = ReadAppSettings("RemoteMachineName");
            string remoteMcUsername = ReadAppSettings("RemoteMcUserName");
            string remoteMcPassword = ReadAppSettings("RemoteMcPassword");

            if (!string.IsNullOrEmpty(remoteMachineName) && !string.IsNullOrEmpty(remoteMcUsername) && !string.IsNullOrEmpty(remoteMcPassword))
            {
                DriveSetting.MapNetworkDrive("P", remoteMachineName, remoteMcUsername, remoteMcPassword);

            }
        }

        private void DisconnectMappedDrive()
        {
        }

        private void ConnectToRemoteMachine()
        {
            string remoteMachineName = ReadAppSettings("RemoteMachineName");
            string remoteMcUsername = ReadAppSettings("RemoteMcUserName");
            string remoteMcPassword = ReadAppSettings("RemoteMcPassword");

            if (!string.IsNullOrEmpty(remoteMachineName) && !string.IsNullOrEmpty(remoteMcUsername) && !string.IsNullOrEmpty(remoteMcPassword))
            {
                RemoteConnection.connectToRemote(remoteMachineName, remoteMcUsername, remoteMcPassword);
            }
        }

        private bool GetNextFileFromSourceDirectoryAddToFileList()
        {
            //Read all the index files in the Input Dir and store the names in a list

            //  string FileDir = ReadAppSettings(configDirectoryPath);
            string FileDir = selectedDirectoryLocation;
            int fileCounter = 0;
            if (Directory.Exists(FileDir))
            {

                //    var txtFiles = Directory.EnumerateFiles(FileDir, "*.txt", SearchOption.AllDirectories);

                //    foreach (string fileName in txtFiles)
                string fileName = FindFirstFile(FileDir, "*.txt");
                {
                    if (fileCounter == 0)
                    {
                        if (MoveFileFromSourceDirToWorkingDir(fileName))
                            return true;
                    }

                }
            }
            else
            {
                MessageBox.Show(Properties.Resources.SourceFolderDoesnotExist, Properties.Resources.Title, MessageBoxButtons.OK);
                this.Close();
            }
            return false;
        }

        private bool MoveFileFromSourceDirToWorkingDir(string fileName)
        {

            if (File.Exists(fileName))
            {
                string workingPathFileName = Path.Combine(WorkingPath, Path.GetFileName(fileName));
                if (File.Exists(workingPathFileName))
                    File.Delete(workingPathFileName);

                if (!OpenPdfFilesToRedact)
                {
                    if (!CheckIfPdfFileOrItsTextFile(fileName))
                    {
                        File.Move(fileName, workingPathFileName);
                        currentFileDetails = new FileDetail(fileName, workingPathFileName);
                        FileDetail fileDetail = new FileDetail(fileName, workingPathFileName);
                        if (!FileList.Contains(fileDetail))
                        {
                            ReadTextFile(fileDetail);
                            FileList.Add(fileDetail);
                            return true;
                        }
                    }
                }
                else
                {
                    File.Move(fileName, workingPathFileName);
                    currentFileDetails = new FileDetail(fileName, workingPathFileName);
                    FileDetail fileDetail = new FileDetail(fileName, workingPathFileName);
                    if (!FileList.Contains(fileDetail))
                    {
                        ReadTextFile(fileDetail);
                        FileList.Add(fileDetail);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Searches for the first file matching to searchPattern in the sepcified path.
        /// </summary>
        /// <param name="path">The path from where to start the search.</param>
        /// <param name="searchPattern">The pattern for which files to search for.</param>
        /// <returns>Either the complete path including filename of the first file found
        /// or string.Empty if no matching file could be found.</returns>
        public static string FindFirstFile(string path, string searchPattern)
        {
            //  string[] files;

            try
            {

                logger.Log(LogLevel.Info, " source path " + path);
                // Exception could occur due to insufficient permission.
                //var  files = Directory.EnumerateFiles(path, searchPattern, SearchOption.TopDirectoryOnly);
                // If matching files have been found, return the first one.
                // if (files != null &&  files.Length > 0)
                if (Directory.Exists(path))
                {
                    //  DirectoryInfo di = new DirectoryInfo(path);
                    foreach (string fileName in Directory.EnumerateFiles(path, searchPattern, SearchOption.AllDirectories))
                    {
                        logger.Log(LogLevel.Info, " in find first file " + fileName);
                        return fileName;
                    }

                    //foreach (string fileName in Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories))
                    //{
                    //    logger.Log(LogLevel.Info, " in find first file " + fileName);
                    //    return fileName;
                    //}

                }
                else
                {
                    // Otherwise find all directories.
                    string[] directories;

                    try
                    {
                        // Exception could occur due to insufficient permission.
                        directories = Directory.GetDirectories(path);
                    }
                    catch (Exception e)
                    {
                        logger.Log(LogLevel.Error, "Exception - " + e.Message);
                        logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                        logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
                        return string.Empty;
                    }

                    // Iterate through each directory and call the method recursively.
                    foreach (string directory in directories)
                    {
                        string file = FindFirstFile(directory, searchPattern);

                        // If we found a file, return it (and break the recursion).
                        if (file != string.Empty)
                        {
                            return file;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, " Exception: FindFirstFile  " + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
                return string.Empty;
            }


            // If no file was found (neither in this directory nor in the child directories)
            // simply return string.Empty.
            return string.Empty;
        }

        public static string ReadAppSettings(string keyValue)
        {
            try
            {
                // Get the AppSettings section.
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                // Get the AppSettings section elements.

                if (appSettings.Count == 0)
                {
                    return string.Empty;
                }
                for (int i = 0; i < appSettings.Count; i++)
                {
                    if (appSettings.GetKey(i) == (keyValue))
                        return appSettings[i];
                }
            }
            catch (ConfigurationErrorsException e)
            {
                logger.Log(LogLevel.Error, " Exception: ReadAppSettings " + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
            return string.Empty;
        }

        private void ReadTextFile(FileDetail fileDetails)
        {
            try
            {


                //  int counter = 0;
                string line;
                //  string sdata = ""; 
                //  int pos;

                using (System.IO.StreamReader streamReader = new System.IO.StreamReader(fileDetails.workingFolderName))
                {

                    if (!Directory.Exists(WorkingPath))
                    {
                        Directory.CreateDirectory(WorkingPath);
                    }

                    ImageID = string.Empty;
                    ImageVersion = string.Empty;
                    Filename = string.Empty;
                    OutPath = string.Empty;
                    FileExt = string.Empty;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        string[] result = line.Split('=');
                        if (result.Length >= 1)
                        {
                            logger.Log(LogLevel.Info, "Line:" + result[0] + "=" + result[1]);

                            if (result[0].Contains("ImageID"))
                                ImageID = result[1];
                            if (result[0] == ("ImageVersion"))
                                ImageVersion = result[1];
                            if (result[0].Contains("ObjectName"))
                                Filename = result[1];
                            if (result[0].Contains("ObjectFullPath"))
                                OutPath = result[1];
                            if (result[0].Contains("ObjectFormat"))
                                FileExt = result[1];
                        }
                        //pos = line.IndexOf("=");
                        //if (pos >= 0)
                        //    sdata = line.Substring(pos + 1, line.Length - (pos + 1));
                        //if (counter == 1)
                        //    Filename = sdata;
                        //if (counter == 3)
                        //  OutPath =  sdata;
                        //if (counter == 4)
                        //    FileExt = sdata;
                        //counter++;
                    }
                    // temp code added to be commented later 
                    //string orgFile;
                    //if (!string.IsNullOrEmpty(fileDetails.imageFileName))
                    //{
                    //    orgFile = Path.Combine(Path.GetDirectoryName(fileDetails.sourceFileName), Path.GetFileName(fileDetails.imageFileName));
                    //    logger.Log(LogLevel.Info, " checking file in location:" + orgFile);
                    //    if (File.Exists(orgFile))
                    //    {
                    //        Filename = orgFile;
                    //    }
                    //    else
                    //    {
                    //        logger.Log(LogLevel.Info, "Image file at location: " + OutPath);

                    //    }
                    //}
                    //else
                    //{
                    //    orgFile = Path.Combine(Path.GetDirectoryName(fileDetails.sourceFileName), Path.GetFileNameWithoutExtension(fileDetails.workingFolderName), ".TIF");
                    //    logger.Log(LogLevel.Info, " checking file in location:" + orgFile);
                    //    if (File.Exists(orgFile))
                    //    {
                    //        Filename = orgFile;
                    //    }
                    //    else
                    //    {
                    //        orgFile = Path.Combine(Path.GetDirectoryName(fileDetails.sourceFileName), Path.GetFileNameWithoutExtension(fileDetails.workingFolderName), ".JPG");
                    //        logger.Log(LogLevel.Info, " checking file in location:" + orgFile);
                    //        if (File.Exists(orgFile))
                    //        {
                    //            Filename = orgFile;
                    //        }
                    //    }


                    //}





                    streamReader.Close();
                    GC.Collect();
                }
                logger.Log(LogLevel.Info, "ObjectFullPath = " + OutPath);
                //  if (inTestingPhase && string.IsNullOrEmpty(OutPath))
                if (inTestingPhase)
                {
                    OutPath = Path.Combine(ReadAppSettings("OutputPath"));
                    logger.Log(LogLevel.Info, "in testing outputpath" + OutPath);
                }

                if (string.IsNullOrEmpty(FileExt))
                {
                    string tempFilePath = Path.Combine(Path.GetDirectoryName(fileDetails.sourceFileName), Path.GetFileNameWithoutExtension(fileDetails.sourceFileName) + ".TIF");
                    logger.Log(LogLevel.Info, tempFilePath);
                    if (File.Exists(tempFilePath))
                    {
                        FileExt = ".TIF";
                    }
                    else
                    {
                        tempFilePath = Path.Combine(Path.GetDirectoryName(fileDetails.sourceFileName), Path.GetFileNameWithoutExtension(fileDetails.sourceFileName) + ".JPG");
                        logger.Log(LogLevel.Info, tempFilePath);
                        if (File.Exists(tempFilePath))
                        {
                            FileExt = ".JPG";
                        }

                    }
                }

                logger.Log(LogLevel.Info, "FileExt" + FileExt);

                logger.Log(LogLevel.Info, " filename = " + Filename + "" + FileExt + "Outpath" + OutPath);
                if (!string.IsNullOrEmpty(Filename) && !string.IsNullOrEmpty((FileExt))) // && !string.IsNullOrEmpty(OutPath))
                {

                    if (!inTestingPhase)
                    {
                        //get the file extension from the output path 
                        FileExt = Path.GetExtension(OutPath);
                        logger.Log(LogLevel.Info, "in testing fileext" + FileExt);
                    }


                    Filename = Path.ChangeExtension(Filename, FileExt);
                    this.Text = thistext + " - " + Filename;
                    //FileFullName = Filename;
                    logger.Log(LogLevel.Info, "Filefullname:" + FileFullName);
                    //   string FileDir = ReadAppSettings(configDirectoryPath);
                    string FileDir = selectedDirectoryLocation;
                    FileFullName = Path.ChangeExtension(fileDetails.workingFolderName, FileExt);

                    // if in testing phase use output path from the configuration else from the txt file available
                    //   if (inTestingPhase && string.IsNullOrEmpty(OutPath))
                    if (inTestingPhase)
                    {
                        OutPath = Path.Combine(ReadAppSettings("OutputPath"), Path.GetFileName(FileFullName));
                    }
                    logger.Log(LogLevel.Info, "Outpath" + OutPath);
                    fileDetails.imageFileName = FileFullName;
                    fileDetails.outputPathName = OutPath;


                    string sourceImageFileName = Path.ChangeExtension(fileDetails.sourceFileName, FileExt);

                    logger.Log(LogLevel.Info, "Source file location:" + sourceImageFileName);
                    // sourceImageFileName = Path.Combine(string.IsNullOrEmpty(sourceLocation) ? FileDir : Path.GetDirectoryName(sourceLocation), Path.GetFileName(FileFullName));
                    if (File.Exists(sourceImageFileName))
                    {

                        if (Directory.Exists(FileDir))
                        {
                            if (File.Exists(FileFullName))
                            {
                                File.Delete(FileFullName);
                            }

                            File.Move(sourceImageFileName, FileFullName);
                            logger.Log(LogLevel.Info, "Temp File location:" + FileFullName);

                        }
                    }
                    else
                    {
                        if (File.Exists(OutPath))
                        {
                            File.Copy(OutPath, FileFullName);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Exception: ReadTextFile" + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
        }

        private void loadImage(string fileFullName)
        {
            try
            {
                // ReadTextFile(fileFullName, null);
                isImageChanged = false;
                _pageCount = 0;
                _currentPage = 0;
                UpdateUi();
                string appName = ReadAppSettings(FileExt.ToLower());
                logger.Log(LogLevel.Info, "File extension: "+ FileExt + " application assigned = " + appName);
                if (!string.IsNullOrEmpty(appName))
                //    if (FileExt.ToLower() == ".pdf" || Path.GetExtension(fileFullName).ToLower() == ".pdf")
                {
                    dateTimeBeforeOpeningFile = System.IO.File.GetLastWriteTime(FileFullName);
                    // OpenPdfFiles();
                    OpenNONImageFiles(appName);
                }
                else
                {
                    

                    OpenNonPdfFile();
                }
            }
            catch (Exception e)
            {

                logger.Log(LogLevel.Error, "loadImage:" + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }

        }

        private async void OpenNonPdfFile()
        {
            try
            {


                if (File.Exists(FileFullName))
                {

                    await Task.Run(() => SplitIntoPages(FileFullName));
                    // SplitIntoPages(FileFullName);
                    isRedactionDone = false;
                    btnRedAct.Enabled = false;
                    picMain.SizeMode = PictureBoxSizeMode.StretchImage;

                    // OpenImage(sImage);
                    if (_pageCount == 0)
                        picMain.Image = sImage;
                    else
                    {

                        logger.Log(LogLevel.Info, " No of pages in the file  " + _pageCount);
                        ShowPage(1);
                        UpdateUi();


                    }
                    picMain.Refresh();

                    IsLoading = false;
                    logger.Log(LogLevel.Info, " file loaded on the UI ");
                    hScrollBar1.Visible = false;
                    vScrollBar1.Visible = false;
                    //this.DisplayScrollBars();
                    //this.SetScrollBarValues();
                    //vScrollBar1.Height = picMain.Height;
                    //hScrollBar1.Width = picMain.Width;
                    //vScrollBar1.Value = 0;
                    //hScrollBar1.Value = 0;
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Exception: OpenNonPDFFIle:" + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
        }

        private void SplitIntoPages(string FileName)
        {
            try
            {
                UseWaitCursor = true;
                Bitmap bmpImage = null;
                mImage = null;
                using (mImage = Image.FromFile(FileFullName))
                {
                    _openImage = mImage;
                    _pageCount = this.GetPageCount(mImage);
                    _currentPage = 1;
                    bmpImage = new Bitmap(mImage, mImage.Width, mImage.Height);
                    //sImage = new Bitmap(mImage.Width+1, mImage.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    Graphics g = Graphics.FromImage(bmpImage);
                    g.DrawImage(bmpImage, 0, 0);
                    g.Dispose();



                    GC.Collect();

                    sImage = bmpImage;
                    // if (picMain.Image != null) picMain.Dispose();
                    picMain.Image = sImage;
                    //  this.CloseImage();
                    FrameDimension dimension;

                    dimension = FrameDimension.Page;
                    //  _openImage = sImage;


                    if (_pageCount > 1)
                    {
                        string strPath = WorkingPath + "\\" + Path.GetFileNameWithoutExtension(FileName);
                        CurrentPageLocation = strPath;

                        if (!Directory.Exists(strPath))
                        {
                            Directory.CreateDirectory(strPath);
                        }

                        //mImage.SelectActiveFrame(dimension, 0);
                        //string pageName = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + "-Page" + 0 + ".tif";
                        //if (File.Exists(pageName))
                        //{
                        //    File.Delete(pageName);
                        //}
                        //mImage.Save(pageName, ImageFormat.Tiff);



                        //Task.Run(() =>
                        //{
                        //Saves every frame as a seperate file.
                        for (int i = 0; i < _pageCount; i++)
                        {
                            mImage.SelectActiveFrame(dimension, i);
                            string pageName = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + "-Page" + i + ".tif";
                            if (File.Exists(pageName))
                            {
                                File.Delete(pageName);
                            }
                            mImage.Save(pageName, ImageFormat.Tiff);

                        }
                        // });
                        //  ShowPage(1);
                        //  UpdateUi();
                    }
                    //else
                    //{
                    //    picMain.Image = sImage;

                    //  //  this.ShowPage(1);
                    //}




                    GC.Collect();
                    UseWaitCursor = false;
                    Application.DoEvents();
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, "Error in split document " + e);
            }

        }

        private void UpdateFileCounter(int i)
        {
            //Update counter to contrtol next and prev
            FileCounter += i;

            btnPrev.Enabled = true;
            previousToolStripMenuItem.Enabled = true;
            btnDelete.Enabled = true;
            btnEnableRedaction.Enabled = true;

            //if (FileCounter == 0)
            //{
            //    btnPrev.Enabled = false;
            //    previousToolStripMenuItem.Enabled = false;
            //}

            if (FileCounter >= FileList.Count)
            {
                FileCounter = FileList.Count - 1;
            }

            if (FileCounter >= 0 && FileCounter <= FileList.Count - 1)
            {
                currentFileDetails = FileList.ElementAt(FileCounter);
                FileFullName = currentFileDetails.imageFileName;
                Filename = Path.GetFileName(FileFullName);
                FileExt = Path.GetExtension(FileFullName);
                OutPath = currentFileDetails.outputPathName;
                this.Text = thistext + " - " + Filename;
            }
            else
            {
                //FileCounter = FileList.Count - 1;
                //currentFileDetails = FileList.ElementAt(FileCounter);
                //FileFullName = currentFileDetails.imageFileName;
                //Filename = Path.GetFileName(FileFullName);
                //FileExt = Path.GetExtension(FileFullName);
                //OutPath = currentFileDetails.outputPathName;
                //this.Text = thistext + " - " + Filename;

                FileFullName = string.Empty;
                Filename = string.Empty;
            }
        }


        //private void GetFileList(int fileCounter)
        //{
        //    //Read all the index files in the Input Dir and store the names in a list

        //   // string FileDir = ReadAppSettings(configDirectoryPath);
        //    string FileDir = selectedDirectoryLocation;
        //    //int fileCounter = 0;
        //    if (Directory.Exists(FileDir))
        //    {
        //        //GetNextFileFromSourceDirectoryAddToFileList();
        //        //var txtFiles = Directory.EnumerateFiles(FileDir, "*.txt", SearchOption.AllDirectories);
        //        //fileCounter = txtFiles.ToList<string>().Count;
        //        //foreach (string fileName in txtFiles)
        //        //{
        //        //  if (Path.GetExtension(fileName) == ".txt")
        //        //    {
        //        //        fileCounter++;
        //        //    }
        //        //}
        //        fileStatus.Text = fileStatus.Text + fileCounter;
        //    }
        //    else
        //    {
        //        MessageBox.Show(Properties.Resources.SourceFolderDoesnotExist, Properties.Resources.Title, MessageBoxButtons.OK);
        //        this.Close();
        //    }            
        //}

        private void OpenNONImageFiles(string appName)
        {
            btnEnableRedaction.Enabled = false;
            btnRedAct.Enabled = false;
            btnSave.Enabled = true;
            Process myProcess;
            try
            {
                if (File.Exists(FileFullName) && !string.IsNullOrEmpty(appName))
                {
                    myProcess = new Process();
                    myProcess.StartInfo.FileName = appName; //not the full application path
                    myProcess.StartInfo.Arguments = "\"" + FileFullName + "\"";
                    logger.Log(LogLevel.Info, " trying to open file: parameters passed : " + appName + "  file name : " + FileFullName);
                    myProcess.Start();
                    myProcess.WaitForExit();
                    logger.Log(LogLevel.Info, "After closing the "+ appName + " application");
                    btnSave.Enabled = true;
                }
            }
            catch (Exception e)
            {
                myProcess = null;
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
                MessageBox.Show(Properties.Resources.PDFReaderError, Properties.Resources.Title, MessageBoxButtons.OK);
            }
        }

        public void SetScrollBarValues()
        {
            // Set the Maximum, Minimum, LargeChange and SmallChange properties.
            this.vScrollBar1.Minimum = 0;
            this.hScrollBar1.Minimum = 0;
            // If the offset does not make the Maximum less than zero, set its value. 
            if ((this.picMain.Image.Size.Width - picMain.ClientSize.Width) > 0)
            {
                this.hScrollBar1.Maximum = this.picMain.Image.Size.Width - picMain.ClientSize.Width;
            }
            /* If the VScrollBar is visible, adjust the Maximum of the 
               HSCrollBar to account for the width of the VScrollBar. */
            if (this.vScrollBar1.Visible)
            {
                this.hScrollBar1.Maximum += this.vScrollBar1.Width;
            }
            this.hScrollBar1.LargeChange = this.hScrollBar1.Maximum / 10;
            this.hScrollBar1.SmallChange = this.hScrollBar1.Maximum / 20;
            // Adjust the Maximum value to make the raw Maximum value attainable by user interaction.
            this.hScrollBar1.Maximum += this.hScrollBar1.LargeChange;

            // If the offset does not make the Maximum less than zero, set its value.    
            if ((this.picMain.Image.Size.Height - picMain.ClientSize.Height) > 0)
            {
                this.vScrollBar1.Maximum = this.picMain.Image.Size.Height - picMain.ClientSize.Height;
            }
            /* If the HScrollBar is visible, adjust the Maximum of the 
               VSCrollBar to account for the width of the HScrollBar.*/
            if (this.hScrollBar1.Visible)
            {
                this.vScrollBar1.Maximum += this.hScrollBar1.Height;
            }
            this.vScrollBar1.LargeChange = this.vScrollBar1.Maximum / 10;
            this.vScrollBar1.SmallChange = this.vScrollBar1.Maximum / 20;
            // Adjust the Maximum value to make the raw Maximum value attainable by user interaction.
            this.vScrollBar1.Maximum += this.vScrollBar1.LargeChange;
        }


        public void DisplayScrollBars()
        {
            // If the image is wider than the PictureBox, show the HScrollBar.
            if (picMain.Width > picMain.Image.Width - this.vScrollBar1.Width)
            {
                hScrollBar1.Visible = false;
            }
            else
            {
                hScrollBar1.Visible = true;
            }

            // If the image is taller than the PictureBox, show the VScrollBar.
            if (picMain.Height > picMain.Image.Height - this.hScrollBar1.Height)
            {
                vScrollBar1.Visible = false;
            }
            else
            {
                vScrollBar1.Visible = true;
            }
        }


        private void Main_Resize_1(object sender, EventArgs e)
        {
            vScrollBar1.Height = picMain.Height;
            hScrollBar1.Width = picMain.Width;
        }

        private void picMain_MouseDown(object sender, MouseEventArgs e)
        {
            Paint = true;
            sTartX = e.X;
            sTartY = e.Y;
        }

        private void picMain_MouseUp(object sender, MouseEventArgs e)
        {
            int sx = 0;
            int ex = 0;
            int sy = 0;
            int ey = 0;
            gRectangle rect;

            if (Paint && btnRedAct.Enabled)
            {
                btnRotate.Enabled = false;
                Pen penCurrent = new Pen(Color.Black);


                //e.Graphics.DrawRectangle(penCurrent, Rect);

                color = new SolidBrush(Color.White);
                Color mColor = Color.FromArgb(255);
                Graphics g = picMain.CreateGraphics();

                if (sTartX > e.X)
                {
                    sx = e.X;
                    ex = sTartX;
                }
                else
                {
                    sx = sTartX;
                    ex = e.X;
                }

                if (sTartY > e.Y)
                {
                    sy = e.Y;
                    ey = sTartY;
                }
                else
                {
                    sy = sTartY;
                    ey = e.Y;
                }


                Rectangle Rect1 = new Rectangle(sx, sy, ex - sx, ey - sy);

                penCurrent = new Pen(Color.Black);
                g.DrawRectangle(penCurrent, Rect1);
                g.Dispose();

                sx = sx + hScrollBar1.Value;
                ex = ex + hScrollBar1.Value;
                sy = sy + vScrollBar1.Value;
                ey = ey + vScrollBar1.Value;

                //Rectangle Rect = new Rectangle(sx, sy, ex - sx, ey - sy);

                //rect.sRedActx = sx - 5;
                //rect.eRedActx = ex - 5;
                //rect.sRedActy = sy - 5;
                //rect.eRedcty = ey - 5;

                rect.sRedActx = sx;
                rect.eRedActx = ex;
                rect.sRedActy = sy;
                rect.eRedcty = ey;

                rects.Add(rect);

            }
            //Paintrect(e,"U");
            Paint = false;
            btnRotate.Enabled = true;
        }


        private void picMain_MouseMove(object sender, MouseEventArgs e)
        {
            int sx = 0;
            int ex = 0;
            int sy = 0;
            int ey = 0;

            if (Paint && btnRedAct.Enabled)
            {
                Graphics g = picMain.CreateGraphics();

                if (sTartX > e.X)
                {
                    sx = e.X;
                    ex = sTartX;
                }
                else
                {
                    sx = sTartX;
                    ex = e.X;
                }

                if (sTartY > e.Y)
                {
                    sy = e.Y;
                    ey = sTartY;
                }
                else
                {
                    sy = sTartY;
                    ey = e.Y;
                }

                sx = sx + 5;
                ex = ex + 5;
                sy = sy + 5;
                ey = ey + 5;

                Rectangle Rect1 = new Rectangle(sx, sy, ex - sx, ey - sy);
                Pen penCurrent = new Pen(Color.Transparent, 1);
                g.DrawRectangle(penCurrent, Rect1);
                g.Dispose();
            }
        }



        private void CloseImage()
        {
            if (_openImage != null)
            {
                picMain.Image = null;
                _openImage.Dispose();
                _openImage = null;
            }
        }



        private void firstPageToolStripButton_Click(object sender, EventArgs e)
        {

            this.ShowPage(1);
            picMain.Refresh();
            picMain.SizeMode = PictureBoxSizeMode.StretchImage;
            // The ImageBox control doesn't know the image 
            // has changed, so force a repaint
            picMain.Invalidate();
            UpdateUi();
        }

        private int GetPageCount(Image image)
        {
            FrameDimension dimension;

            dimension = FrameDimension.Page;

            return image.GetFrameCount(dimension);
        }

        private void lastPageToolStripButton_Click(object sender, EventArgs e)
        {

            this.ShowPage(_pageCount);
            picMain.Refresh();
            picMain.SizeMode = PictureBoxSizeMode.StretchImage;
            // The ImageBox control doesn't know the image 
            // has changed, so force a repaint
            picMain.Invalidate();
            UpdateUi();
        }

        private void nextPageToolStripButton_Click(object sender, EventArgs e)
        {

            this.ShowPage(_currentPage + 1);
            picMain.Refresh();
            picMain.SizeMode = PictureBoxSizeMode.StretchImage;
            // The ImageBox control doesn't know the image 
            // has changed, so force a repaint
            picMain.Invalidate();
            UpdateUi();

        }

        private Bitmap OpenImage(string filePath)
        {
            Bitmap bmpImage = null;
            Image pageImage = null;
            using (pageImage = Image.FromFile(filePath))
            {


                bmpImage = new Bitmap(pageImage, pageImage.Width, pageImage.Height);
                //sImage = new Bitmap(mImage.Width+1, mImage.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics g = Graphics.FromImage(bmpImage);
                g.DrawImage(bmpImage, 0, 0);
                g.Dispose();

            }

            GC.Collect();
            return bmpImage;
        }

        private void previousPageToolStripButton_Click(object sender, EventArgs e)
        {

            this.ShowPage(_currentPage - 1);
            picMain.Refresh();
            picMain.SizeMode = PictureBoxSizeMode.StretchImage;
            // The ImageBox control doesn't know the image 
            // has changed, so force a repaint
            picMain.Invalidate();
            UpdateUi();
        }




        private void ShowPage(int page)
        {
            _currentPage = page;
            if (_pageCount > 1)
            {
                rects.Clear();
                btnSave.Enabled = isImageChanged;
                btnDelete.Enabled = !btnSave.Enabled;
                btnRedAct.Enabled = false;

                string currentPageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page" + (page - 1) + ".tif";
                if (File.Exists(currentPageLocation))
                {
                    sImage = OpenImage(currentPageLocation);
                    if (sImage != null)
                    {
                        picMain.Image = sImage;

                    }
                }
            }
            // Control.BeginInvoke(UpdateUi());
            picMain.Refresh();
            this.UpdateUi();
        }

        private void UpdateUi()
        {
            bool canMovePrevious;
            bool canMoveNext;

            pageToolStripStatusLabel.Text = $"Pages: {_currentPage} of {_pageCount}";

            canMovePrevious = _currentPage > 1;
            canMoveNext = _currentPage < _pageCount;

            firstPageToolStripButton.Enabled = canMovePrevious;

            previousPageToolStripButton.Enabled = canMovePrevious;

            nextPageToolStripButton.Enabled = canMoveNext;

            lastPageToolStripButton.Enabled = canMoveNext;

        }

        private async Task SaveNONImageFile()
        {
            try
            {
                logger.Log(LogLevel.Info, "output path : " + OutPath + "file exist : " + File.Exists(OutPath));
                if ( File.Exists(FileFullName))
                {
                    File.Copy(FileFullName, OutPath, true);
                    logger.Log(LogLevel.Info, " replaced the redacted file :" + OutPath);
                    if (!inTestingPhase)
                    {
                        if (isDailySelected)
                            UpdateDailyDatabase("Save");
                        else
                            UpdateHistoricDatabase("Save");
                    }
                }
                else
                {
                    MoveFilesToErrorFolder(FileFullName);
                    if (!inTestingPhase)
                    {
                        if (isDailySelected)
                            UpdateDailyDatabase("Deleted");
                        else
                            UpdateHistoricDatabase("Deleted");
                    }
                }

                if (!string.IsNullOrEmpty(ImageID) && !inTestingPhase)
                    ExecuteStoreProcedureToSetImageSizeZero(ImageID, ImageVersion);
                else
                    logger.Log(LogLevel.Info, " In Test mode OR Image ID is null - cant execute Store Procedure");
                DeleteSelectedFile(FileFullName);
                //picMain.Image.Dispose();



               
            }
            catch(Exception e)
            {
                logger.Log(LogLevel.Error, "Error in SaveNONImageFile");
                logger.Log(LogLevel.Error, e.Message);
                logger.Log(LogLevel.Error, e.InnerException);
                logger.Log(LogLevel.Error, e.StackTrace);
            }
        }

        private void MoveFilesToErrorFolder(string fileName)
        {
            try
            {


                string errorFolderPath =
                    Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Error");

                if (!Directory.Exists(errorFolderPath))
                {
                    Directory.CreateDirectory(errorFolderPath);
                }

                if (Directory.Exists(errorFolderPath))
                {
                    string textFileFullName = string.Empty;
                    string selectedFileName = fileName;
                    string txtExt = ".txt";
                    int pos = selectedFileName.IndexOf(".");

                    if (pos >= 0)
                        textFileFullName = selectedFileName.Replace(FileExt, txtExt);


                    FileInfo fi = new FileInfo(textFileFullName);
                    if (!IsFileLocked(fi))
                    {
                        File.Move(textFileFullName, Path.Combine(errorFolderPath, Path.GetFileName(textFileFullName)));
                    }

                    fi = new FileInfo(selectedFileName);
                    if (!IsFileLocked(fi))
                    {
                        File.Move(selectedFileName, Path.Combine(errorFolderPath, Path.GetFileName(selectedFileName)));
                    }
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "MoveFilesToErrorFolder :" + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
        }

        private async Task SaveFile()
        {
            

            if (Directory.Exists(Path.GetDirectoryName(OutPath)) && sImage != null)
            {
                this.UseWaitCursor = true;
                Application.DoEvents();
                //string appName = ReadAppSettings(FileExt.ToLower());
                //logger.Log(LogLevel.Info, "File extension: " + FileExt + " application assigned = " + appName);
                //if (!string.IsNullOrEmpty(appName))
                ////    if (Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                //{
                //    logger.Log(LogLevel.Info, "Found file" + FileFullName);
                //   await  SaveNONImageFile();
                //}
                //else
                {
                    if (_pageCount > 1)
                    {
                        await Task.Run(() => StitchTiffPages(FileFullName, OutPath));
                        //  StitchTiffPages(OutPath);
                    }
                    else
                    {
                        using (System.Drawing.Bitmap sourceBitmap = new System.Drawing.Bitmap(sImage))
                        {
                            string extension = Path.GetExtension(OutPath).ToLower();
                            if (extension.Contains("tif") || extension.Contains("tiff"))
                                sourceBitmap.Save(OutPath, System.Drawing.Imaging.ImageFormat.Tiff);
                            else if (extension.Contains("bmp") || extension.Contains("tiff"))
                                sourceBitmap.Save(OutPath, System.Drawing.Imaging.ImageFormat.Bmp);
                            else if (extension.Contains("jpeg") || extension.Contains("jpg"))
                                sourceBitmap.Save(OutPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            else if (extension.Contains("png") || extension.Contains("tiff"))
                                sourceBitmap.Save(OutPath, System.Drawing.Imaging.ImageFormat.Png);
                            else
                            {
                                sourceBitmap.Save(OutPath, System.Drawing.Imaging.ImageFormat.Tiff);
                            }
                        }
                    }
                    // RemoveFileFromFileList();
                    UpdateFileCounter();
                    if (!inTestingPhase)
                    {
                        if (isDailySelected)
                            UpdateDailyDatabase("Save");
                        else
                            UpdateHistoricDatabase("Save");
                    }


                }

                TotalFilesProcessed++;
                // temporary code added
                if (!string.IsNullOrEmpty(ImageID) && !inTestingPhase)
                    ExecuteStoreProcedureToSetImageSizeZero(ImageID, ImageVersion);
                else
                    logger.Log(LogLevel.Info, " In Test mode OR Image ID is null - cant execute Store Procedure");
                DeleteSelectedFile(FileFullName);
                //picMain.Image.Dispose();

               
            }
            else
            {
                string fileName = FileFullName;
                MoveFilesToErrorFolder(fileName);
                RemoveFileFromFileList();
                UpdateFileCounter();
              //  OpenNextImage(0);
                if (!inTestingPhase)
                {
                    if (isDailySelected)
                        UpdateDailyDatabase("Deleted");
                    else
                        UpdateHistoricDatabase("Deleted");
                }
                MessageBox.Show(Properties.Resources.OutputFolderDoesnotExist, Properties.Resources.Title, MessageBoxButtons.OK);
            }
        }


        public async void StitchTiffPages(string fileName, string destLocation)
        {
            logger.Log(LogLevel.Info, " Starting to stitch tiff pages ");

            ImageCodecInfo myImageCodecInfo;
            Encoder saveEncoder;
            Encoder compressionEncoder;
            EncoderParameter SaveEncodeParam;
            EncoderParameter CompressionEncodeParam;
            EncoderParameters EncoderParams = new EncoderParameters(2);

            saveEncoder = Encoder.SaveFlag;
            compressionEncoder = Encoder.Compression;
            //Get an ImageCodecInfo object that represents the TIFF codec.
            myImageCodecInfo = GetEncoderInfo("image/tiff");
            // Save the first page (frame).
            SaveEncodeParam = new EncoderParameter(saveEncoder, (long)EncoderValue.MultiFrame);
            CompressionEncodeParam = new EncoderParameter(compressionEncoder, (long)EncoderValue.CompressionCCITT4);
            EncoderParams.Param[0] = CompressionEncodeParam;
            EncoderParams.Param[1] = SaveEncodeParam;

            string destFile = destLocation;

            string PageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page0.tif";

            if (File.Exists(PageLocation))
            {
                System.Drawing.Image DestinationImage = (System.Drawing.Image)(new System.Drawing.Bitmap(PageLocation));

                DestinationImage.Save(destFile, myImageCodecInfo, EncoderParams);

                try
                {
                    //  await Task.Run(() =>
                    //  {


                    for (int i = 1; i < _pageCount; i++)
                    {
                        PageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page" + i + ".tif";

                        SaveEncodeParam = new EncoderParameter(saveEncoder, (long)EncoderValue.FrameDimensionPage);
                        CompressionEncodeParam = new EncoderParameter(compressionEncoder, (long)EncoderValue.CompressionCCITT4);
                        EncoderParams.Param[0] = CompressionEncodeParam;
                        EncoderParams.Param[1] = SaveEncodeParam;
                        if (File.Exists(PageLocation))
                        {
                            System.Drawing.Image img = (System.Drawing.Image)(new System.Drawing.Bitmap(PageLocation));
                            DestinationImage.SaveAdd(img, EncoderParams);
                            img.Dispose();

                        }


                    }

                    SaveEncodeParam = new EncoderParameter(saveEncoder, (long)EncoderValue.Flush);
                    EncoderParams.Param[0] = SaveEncodeParam;
                    DestinationImage.SaveAdd(EncoderParams);
                    EncoderParams.Dispose();
                    DestinationImage.Dispose();
                    //  return DestinationImage;
                    if (File.Exists(destFile))
                    {
                        logger.Log(LogLevel.Info, "File created at output location" + destFile);
                        DeleteSelectedFile(FileFullName);

                    }


                    //    });


                    //            DeleteSelectedFile(fileName);
                    //        });


                }
                catch (Exception e)
                {
                    logger.Log(LogLevel.Error, e.InnerException);

                }

            }


        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {

            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (int i = 0; i < encoders.Length; i++)
            {
                if (encoders[i].MimeType == mimeType)
                    return encoders[i];

            }
            return null;
        }

        public void ExecuteStoreProcedureToSetImageSizeZero(string ImageId, string ImageVersion)
        {
            //  SqlDataReader myReader = null;


            try
            {
                logger.Log(LogLevel.Info, " Values passed ImageId=" + ImageId + "ImageVersion " + ImageVersion);
                // connect to the database 
                // SqlConnection sqlConnection = null;

                if (dbConnection == null)
                {
                    logger.Log(LogLevel.Info, "Found connection object null - retrying to set the connection");
                    Connect();
                }
                //   SqlConnection sqlConnection = dbConnection;

                using (dbConnection)
                {

                    if (dbConnection == null)
                    {
                        logger.Log(LogLevel.Error, " SQL Connection could not be made , database not updated ");
                        return;

                    }

                    // execute the stored procedure 

                    //if (sqlConnection.State == ConnectionState.Closed)
                    //{
                    //    logger.Log(LogLevel.Info, "Connection status closed ..trying to open connection again..");
                    //    sqlConnection.Open();
                    //}

                    logger.Log(LogLevel.Debug, "Executing the store procedure ");
                    string strStoreProcedure2 = ReadAppSettings("StoreProcedure2");


                    string query = "exec " + strStoreProcedure2 + "'" + ImageId + "', '" + ImageVersion + "', null";

                    // 1.  create a command object identifying the stored procedure
                    logger.Log(LogLevel.Info, "create a command object identifying the stored procedure" + query);

                    SqlCommand cmd = new SqlCommand(query, dbConnection);

                    //// 2. set the command object so it knows to execute a stored procedure
                    //logger.Log(LogLevel.Info, "set the command object so it knows to execute a stored procedure");
                    //cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = strStoreProcedure2;
                    // 3. add parameter to command, which will be passed to the stored procedure
                    //logger.Log(LogLevel.Info, "add parameter to command, which will be passed to the stored procedure" );
                    //cmd.Parameters.Add(new SqlParameter("@ImageID", ImageId));
                    //cmd.Parameters.Add(new SqlParameter("@ImageVersion",ImageVersion ));
                    //cmd.Parameters.Add(new SqlParameter("@debug", null) );
                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        logger.Log(LogLevel.Info, "Number of records affected " + rdr.RecordsAffected);
                    }

                    dbConnection.Close();
                    // sqlConnection = null;
                }
                dbConnection = null;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "---------------ExecuteStoreProcedureToSetImageSizeZero------------------------");
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);

            }

        }




        private void RemoveFileFromFileList()
        {
            foreach (FileDetail fileDetail in FileList)
            {
                if (fileDetail.imageFileName == FileFullName)
                {
                    FileList.Remove(fileDetail);
                    // FileCounter--;
                    break;
                }
            }
        }

        private void UpdateFileCounter()
        {
            // string FileDir = ReadAppSettings(configDirectoryPath);
            string FileDir = selectedDirectoryLocation;
            int fileCounter = 0;
            if (Directory.Exists(FileDir))
            {

                fileCounter = Directory.EnumerateFiles(FileDir, "*.txt", SearchOption.AllDirectories).ToList().Count;
                //  fileCounter = txtFiles.ToList().Count;

                //ValueType when folder opens initially
                //  fileCounter = selectedFolderCount; 
                //foreach (string fileName in txtFiles)
                //{
                //    if (Path.GetExtension(fileName) == ".txt")
                //    {
                //        fileCounter++;
                //    }
                //}
                // fileStatus.Text = "Processing Files " + (FileCounter + 1).ToString() + " of " + (fileCounter-TotalFilesProcessed).ToString();
                fileStatus.Text = "Processing Files " + (FileCounter + 1).ToString() + " of " + (fileCounter).ToString();
                OpenFilesStatus.Text = "Files in working folder " + FileList.Count;
            }

        }


        public bool Connect()
        {
            if (dbConnection != null)
                return true;

            string databaseName = string.Empty;

            string serverName = string.Empty;

            string userName = string.Empty;

            string password = string.Empty;
            logger.Log(LogLevel.Debug, "Connecting to database");
            serverName = ReadAppSettings("Server");
            databaseName = ReadAppSettings("Database");
            userName = ReadAppSettings("UserId");
            password = ReadAppSettings("Password");

            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(databaseName))
                dbConnection = CreateConnectionString(userName, password, serverName, databaseName);


            //if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(databaseName) &&
            //    !string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            //{
            //    dbConnection =
            //        new SqlConnection("Server=" + serverName + ";Database=" + databaseName + ";User Id=" + userName +
            //                          ";Password=" + password + ";MultipleActiveResultSets=true");
            //}
            if (dbConnection != null)
            {
                logger.Log(LogLevel.Debug, "Connecting to the database");
                return true;
            }
            else
            {
                logger.Log(LogLevel.Info, "Error in establishing SQL Connection ");
                return false;
            }

        }

        private SqlConnection CreateConnectionString(string userId, string password, string server, string database)
        {
            try
            {

                SqlConnectionStringBuilder sqlConnectionStringbuilder = new SqlConnectionStringBuilder();
                sqlConnectionStringbuilder.DataSource = server;
                sqlConnectionStringbuilder.InitialCatalog = database;

                if (string.IsNullOrEmpty(userId) && string.IsNullOrEmpty(password))
                {
                    //sqlConnectionStringbuilder.TrustServerCertificate = true;
                    logger.Log(LogLevel.Info, "logging into sql using windows authentication");
                    // this allows for windows authentication 
                    sqlConnectionStringbuilder.IntegratedSecurity = true;
                }
                else
                {
                    sqlConnectionStringbuilder.UserID = userId;
                    sqlConnectionStringbuilder.Password = password;
                }

                string connectionString = sqlConnectionStringbuilder.ConnectionString;
                logger.Log(LogLevel.Info, " connection string passed " + connectionString);
                Console.WriteLine("connection String " + connectionString);
                var sqlConnection = new SqlConnection(connectionString);

                logger.Log(LogLevel.Info, "made sql connection successfully");

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    logger.Log(LogLevel.Info, "connection closed trying to open sql connection");
                    sqlConnection.Open();
                    logger.Log(LogLevel.Info, " SQL Connection opened ");
                }
                return sqlConnection;
            }
            catch (Exception e)
            {
                // logger.Log(LogLevel.Error, "Error in creating establishing sql connection " + e.StackTrace);
                logger.Log(LogLevel.Error, "---------------CreateConnectionString------------------------");
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
            return null;
        }



        private void UpdateHistoricDatabase(string operation)
        {
            if (dbConnection == null)
                Connect();

            if (dbConnection != null)
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = null;
                try
                {

                    string CheckRecordQuery = "Select * from "+ HistoricTableName +" where ObjectFullPath = '" + OutPath + "'";
                    logger.Log(LogLevel.Info, "select query - updatehistoricdatabase:"+ CheckRecordQuery);
                    myCommand = new SqlCommand(CheckRecordQuery, dbConnection);
                
                    myReader = myCommand.ExecuteReader();
                    if (myReader.HasRows)
                    {
                        string strUpdateQuery;

                        if (operation == "Save")
                            strUpdateQuery = "update "+ HistoricTableName +" set status = 'S', comments = 'ManualRedaction:Redaction successful' where ObjectFullPath = '" + OutPath + "'";
                        else
                            strUpdateQuery = "update "+ HistoricTableName +" set status ='S', comments = 'ManualRedaction:Nothing to redact' where ObjectFullPath = '" + OutPath + "'";

                        logger.Log(LogLevel.Info, "update query - updatehistoricdatabase:" + strUpdateQuery);
                        myReader.Close();
                        myReader = null;
                       
                        myCommand = new SqlCommand(strUpdateQuery, dbConnection);
                        myReader = myCommand.ExecuteReader();
                       
                        logger.Log(LogLevel.Debug, "Updating database ");
                    }
                    else
                    {
                        logger.Log(LogLevel.Info, " No table updated-no record found in " + HistoricTableName + " table for Outpath " + OutPath);
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                        myReader = null;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    logger.Log(LogLevel.Error, "Exception - " + e.Message);
                    logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                    logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
                }

            }
            else
            {
                //update log file 
                logger.Log(LogLevel.Error, "Connection to database could not be established ");
            }
        }


        private void UpdateDailyDatabase(string operation)
        {
            if (dbConnection == null)
                Connect();
            if (dbConnection != null)
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = null;
                try
                {

                    string CheckRecordQuery = "Select * from "+DailyTableName+" where ObjectFullPath = '" + OutPath + "'";
                    logger.Log(LogLevel.Info, "select query - updatedailydatabase:" + CheckRecordQuery);

                    myCommand = new SqlCommand(CheckRecordQuery, dbConnection);
                    myReader = myCommand.ExecuteReader();
                    if (myReader.HasRows)
                    {
                        string strUpdateQuery;

                        if (operation == "Save")
                            strUpdateQuery = "update "+DailyTableName+" set status = 'S', comments = 'ManualRedaction:Redaction successful' where ObjectFullPath = '" + OutPath + "'";
                        else
                            strUpdateQuery = "update "+DailyTableName+" set status ='S', comments = 'ManualRedaction:Nothing to redact' where ObjectFullPath = '" + OutPath + "'";

                        myReader.Close();
                        myReader = null;
                        logger.Log(LogLevel.Info, "select query - updatedailydatabase:" + strUpdateQuery);

                        myCommand = new SqlCommand(strUpdateQuery, dbConnection);
                        myReader = myCommand.ExecuteReader();
                        logger.Log(LogLevel.Debug, "Updating database ");
                    }
                    else
                    {
                        logger.Log(LogLevel.Info, " No table updated-no record found in "+DailyTableName+" table for Outpath " + OutPath);
                    }
                    if (myReader != null)
                    {
                        myReader.Close();
                        myReader = null;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    logger.Log(LogLevel.Error, "Exception - " + e.Message);
                    logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                    logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
                }

            }
            else
            {
                //update log file 
                logger.Log(LogLevel.Error, "Couldnt establish connection with Database - cannot perform database operations"); 
            }
        }

        private void RotateImage()
        {
            picMain.Image.RotateFlip(RotateFlipType.Rotate90FlipXY);
            picMain.Refresh();
            logger.Log(LogLevel.Debug, "Image rotated");
        }

        private void RedActImage()
        {
            isImageChanged = true;
            btnSave.Enabled = true;

            vScrollValue = vScrollBar1.Value;
            hScrollValue = hScrollBar1.Value;
            foreach (gRectangle rect in rects)
            {
                for (int j = rect.sRedActy; j <= rect.eRedcty; j++)
                {
                    for (int x = rect.sRedActx; x <= rect.eRedActx; x++)
                    {
                        if (x < sImage.Width && 0 <= j && j < sImage.Height)
                            sImage.SetPixel(x, j, Color.White);
                    }
                }
            }

            logger.Log(LogLevel.Debug, "Image redacted");
            if (_pageCount > 1)
            {
                string pageLocation = CurrentPageLocation + "\\" + Path.GetFileNameWithoutExtension(FileFullName) + "-Page" + (_currentPage - 1) + ".tif";
                if (File.Exists(Path.Combine(pageLocation)))
                {
                    File.Delete(pageLocation);
                }

                sImage.Save(pageLocation, ImageFormat.Tiff);
            }
            picMain.Image = sImage;
            picMain.Refresh();
            vScrollBar1.Value = vScrollValue;
            hScrollBar1.Value = hScrollValue;
            vScrollBar1_Scroll(null, null);
            hScrollBar1_Scroll(null, null);
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            /* Create a graphics object and draw a portion of the image in the PictureBox. */
            if (picMain.Image != null)
            {
                Graphics g = picMain.CreateGraphics();
                g.DrawImage(picMain.Image,
                            new Rectangle(0, 0, picMain.Right - vScrollBar1.Width,
                                          picMain.Bottom - hScrollBar1.Height),
                            new Rectangle(hScrollBar1.Value, vScrollBar1.Value,
                                          picMain.Right - vScrollBar1.Width,
                                          picMain.Bottom - hScrollBar1.Height),
                            GraphicsUnit.Pixel);
                vScrollValue = vScrollBar1.Value;
                picMain.Update();
            }

        }

        private void OpenPreviousImage(int imageCounter)
        {
            UpdateFileCounter(imageCounter);
            UpdateFileCounter();
            picMain.Image = null;
            btnRotate.Enabled = true;
            btnEnableRedaction.Enabled = true;
            btnDelete.Enabled = true;
            btnSave.Enabled = false;
            btnRedAct.Enabled = false;

            rects.Clear();
            if (!string.IsNullOrEmpty(FileFullName))
            {
                logger.Log(LogLevel.Info, " opening previous image  " + FileFullName);

                loadImage(FileFullName);
            }
            logger.Log(LogLevel.Debug, "Previous image loaded");
        }

        private void OpenNextImage(int imageCounter)
        {
            btnRotate.Enabled = true;
            btnEnableRedaction.Enabled = true;
            btnDelete.Enabled = true;
            btnSave.Enabled = false;
            btnRedAct.Enabled = false;

            if (FileCounter >= FileList.Count - 1)
            {


                if (GetNextFileFromSourceDirectoryAddToFileList())
                {

                    UpdateFileCounter(imageCounter);


                    // set Slider Attributes
                    // previousScrollValue = 1;
                    //zoomSlider.Minimum = 1;
                    //zoomSlider.Maximum = 5;
                    //zoomSlider.SmallChange = 1;
                    //zoomSlider.LargeChange = 1;
                    //zoomSlider.UseWaitCursor = false;

                    // reduce flickering
                    //this.DoubleBuffered = true;

                    picMain.Image = null;

                    rects.Clear();
                    if (!string.IsNullOrEmpty(FileFullName))
                    {
                        logger.Log(LogLevel.Debug, "Opening next image " + FileFullName);

                        loadImage(FileFullName);
                    }
                    UpdateFileCounter();
                    picMain.SizeMode = PictureBoxSizeMode.StretchImage;
                    logger.Log(LogLevel.Debug, "Opened next image");
                }
                else
                {

                    MessageBox.Show(Properties.Resources.NoImagesAvailable, Properties.Resources.Title,
                                    MessageBoxButtons.OK);
                    logger.Log(LogLevel.Debug, "filecounter " + FileCounter);
                    //  if (FileList.Count <= 0)
                    try
                    {
                       
                        btnRotate.Enabled = false;
                        btnEnableRedaction.Enabled = false;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = false;
                        btnRedAct.Enabled = false;
                        if(picMain.Image != null)
                        {
                            picMain.Image.Dispose();
                        }
                        
                        picMain.Image = null;
                    }
                    catch (Exception e)
                    {
                        logger.Log(LogLevel.Debug, " Error in setting values to default" + e.InnerException);
                    }

                }
            }
            else
            {
                UpdateFileCounter(imageCounter);
                UpdateFileCounter();
                picMain.Image = null;

                rects.Clear();
                if (!string.IsNullOrEmpty(FileFullName))
                {
                    loadImage(FileFullName);
                }
            }
        }

        private void DeleteSelectedFile(string fileName)
        {
            try
            {
                if (!string.IsNullOrEmpty(CurrentPageLocation) && Directory.Exists(CurrentPageLocation))
                    Directory.Delete(CurrentPageLocation, true);
                const string txtExt = ".txt";
                string textFileFullName = string.Empty;
                string selectedFileName = fileName;

                textFileFullName = Path.ChangeExtension(selectedFileName, txtExt);

                RemoveFileFromFileList();

                //if (File.Exists(textFileFullName))
                //{
                //    logger.Log(LogLevel.Info, "Archiving file" + textFileFullName);
                //    File.Move(textFileFullName,Path.Combine(ArchiveFolderPath, Path.GetFileName(textFileFullName)));
                //}


                GC.Collect();


                //if (File.Exists(selectedFileName))
                //{
                //        logger.Log(LogLevel.Info, "Deleting file :"+ selectedFileName);
                //    File.Delete(selectedFileName);
                //}
                // moving the image file to archive folder if flag set to archive= true
                if (File.Exists(Path.Combine(WorkingPath, Path.GetFileName(selectedFileName))))
                {
                    logger.Log(LogLevel.Info, "Archiving file" + ArchiveFiles + " file "+ Path.Combine(WorkingPath, Path.GetFileName(selectedFileName))+ "to "+ Path.Combine(ArchiveFolderPath, Path.GetFileName(selectedFileName)));
                    if (ArchiveFiles)
                    {
                        File.Copy(Path.Combine(WorkingPath, Path.GetFileName(selectedFileName)), Path.Combine(ArchiveFolderPath, Path.GetFileName(selectedFileName)), true);
                    }
                    
                        File.Delete(Path.Combine(WorkingPath, Path.GetFileName(selectedFileName)));
                    logger.Log(LogLevel.Info, " files deleted " + Path.Combine(WorkingPath, Path.GetFileName(selectedFileName)));
                }

                // copying the text file to archive folder 
                if (File.Exists(Path.Combine(WorkingPath, Path.GetFileName(textFileFullName))))
                {
                    logger.Log(LogLevel.Info, "Archivingflag "+ ArchiveFiles + " file" + Path.Combine(WorkingPath, Path.GetFileName(textFileFullName))+ " to " + Path.Combine(ArchiveFolderPath, Path.GetFileName(textFileFullName)));

                    if (File.Exists(Path.Combine(WorkingPath, Path.GetFileName(textFileFullName))))
                    {
                        if (ArchiveFiles)
                        {
                            File.Copy(Path.Combine(WorkingPath, Path.GetFileName(textFileFullName)), Path.Combine(ArchiveFolderPath, Path.GetFileName(textFileFullName)), true);
                        }
                        File.Delete(Path.Combine(WorkingPath, Path.GetFileName(textFileFullName)));
                    }
                }

                GC.Collect();
                logger.Log(LogLevel.Info, " files deleted " + textFileFullName);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
        }

        /// This function is used to check specified file being used or not        
        /// </summary>        
        /// <param name="file">FileInfo of required file</param>       
        /// <returns>If that specified file is being processed         
        /// or not found is return true</returns>        
        public static Boolean IsFileLocked(FileInfo file)
        {

            FileStream stream = null;
            try
            {
                if (File.Exists(file.FullName))
                {
                    //Don't change FileAccess to ReadWrite,      
                    //because if a file is in readOnly, it fails.   
                    stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
                }
            }
            catch (IOException e)
            {
                //the file is unavailable because it is: 
                //still being written to               
                //or being processed by another thread   
                //or does not exist (has already been processed) 
                //logger.Log(LogLevel.Info, " isFileLocked " + e.StackTrace);
                logger.Log(LogLevel.Info, "File is in use- " + e.Message);
                logger.Log(LogLevel.Info, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Info, "StackTrace - " + e.StackTrace);
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();

            }
            //file is not locked
            return false;
        }

        private void previousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isImageChanged)
            {
                DialogResult result = MessageBox.Show(Properties.Resources.MoveToNextImage, Properties.Resources.Title,
                                  MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    OpenPreviousImage(-1);

                    if (Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                    {
                        logger.Log(LogLevel.Info, "File is PDF" + FileFullName);
                        btnSave.Enabled = true;
                     }
                    else
                    {
                        logger.Log(LogLevel.Info, "file not a pdf " + FileFullName);
                        isImageChanged = false;
                        btnSave.Enabled = false;

                    }
                }
            }
            else
            {
                OpenPreviousImage(-1);

                if (!Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                {
                    logger.Log(LogLevel.Info, "file not a pdf " + FileFullName);
                    isImageChanged = false;
                    btnSave.Enabled = false;
                }
                else
                {
                    logger.Log(LogLevel.Info, "File is PDF" + FileFullName);
                    btnSave.Enabled = true;
                }
            }

        }

        private void nextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isImageChanged)
            {
                DialogResult result = MessageBox.Show(Properties.Resources.MoveToNextImage, Properties.Resources.Title,
                                  MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    OpenNextImage(1);
                    if (!string.IsNullOrEmpty(FileFullName))
                    {
                        if (Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                        {
                            logger.Log(LogLevel.Info, "File is PDF" + FileFullName);
                            btnSave.Enabled = true;

                        }
                        else
                        {
                            logger.Log(LogLevel.Info, "file not a pdf " + FileFullName);
                            isImageChanged = false;
                            btnSave.Enabled = false;
                        }
                    }
                }
            }
            else
            {
                OpenNextImage(1);

                if (!string.IsNullOrEmpty(FileFullName))
                {
                    logger.Log(LogLevel.Info, Path.GetExtension(FileFullName).ToLower());

                    if (Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                    {
                        logger.Log(LogLevel.Info, "File is PDF" + FileFullName);
                        btnSave.Enabled = true;

                    }
                    else
                    {
                        logger.Log(LogLevel.Info, "file not a pdf " + FileFullName);
                        isImageChanged = false;
                        btnSave.Enabled = false;

                    }
                }
            }

        }

        private void redActToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileFullName) && Path.GetExtension(FileFullName) != ".pdf")
            {
                RedActImage();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveSelectedFile();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DeleteFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rotateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileFullName) && Path.GetExtension(FileFullName) != ".pdf")
            {
                RotateImage();
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (FileCounter != 0)
            {
                previousToolStripMenuItem_Click(sender, e);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            nextToolStripMenuItem_Click(sender, e);
        }

        private void btnRotate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileFullName) && Path.GetExtension(FileFullName) != ".pdf")
            {
                RotateImage();
            }
        }

        private void btnRedAct_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(FileFullName) && Path.GetExtension(FileFullName) != ".pdf")
            {
                RedActImage();
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileFullName))
            {
                UndoChanges();
            }
        }

        private void UndoChanges()
        {
            isImageChanged = false;
            btnSave.Enabled = false;
            btnDelete.Enabled = true;
            rects.Clear();
            string appName = ReadAppSettings(FileExt.ToLower());
            logger.Log(LogLevel.Info, "File extension: " + FileExt + " application assigned = " + appName);
            if (!string.IsNullOrEmpty(appName))
            //    if (FileExt == ".pdf")
            {
                OpenNONImageFiles(appName);
            }
            else
            {
                if (!string.IsNullOrEmpty(CurrentPageLocation) && Directory.Exists(CurrentPageLocation))
                    Directory.Delete(CurrentPageLocation, true);

                picMain.Image = null;
                OpenNonPdfFile();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

           SaveSelectedFile();

        }

        private async void SaveSelectedFile()
        {

            if (!string.IsNullOrEmpty(FileFullName))
            {
                DialogResult result = MessageBox.Show(Properties.Resources.SaveConfirmationMsg, Properties.Resources.Title, MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {


                    logger.Log(LogLevel.Info, "Confirmed File for redaction " + FileFullName);
                    string appName = ReadAppSettings(FileExt.ToLower());
                    logger.Log(LogLevel.Info, "File extension: " + FileExt + " application assigned = " + appName);
                    if (!string.IsNullOrEmpty(appName))
                    //    if (Path.GetExtension(FileFullName).ToLower().Contains("pdf"))
                    {
                        logger.Log(LogLevel.Info, "Found file" + FileFullName);
                       
                        await SaveNONImageFile();
                        logger.Log(LogLevel.Info, "file saved to image right location- SaveNONImageFile");
                        // DeleteSelectedFile(FileFullName);
                        OpenNextImage(0);
                    }
                    else
                    {

                       await SaveFile();
                        logger.Log(LogLevel.Info, " file saved to image right location-Save File");
                        //  DeleteSelectedFile(FileFullName);
                        OpenNextImage(0);
                        isImageChanged = false;
                        this.UseWaitCursor = false;
                    }
                 
                }
            }
        }

        // deletes the file 
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFile();
        }

        private void DeleteFile()
        {
            if (!string.IsNullOrEmpty(FileFullName))
            {
                DialogResult result = MessageBox.Show(Properties.Resources.ReleaseWOChangeConfirmationMsg,
                                                      Properties.Resources.Title, MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    DeleteSelectedFile(FileFullName);
                    UpdateFileCounter();
                    if (!inTestingPhase)
                    {
                        if (isDailySelected)
                            UpdateDailyDatabase("Deleted");
                        else
                            UpdateHistoricDatabase("Deleted");
                    }
                    TotalFilesProcessed++;
                    OpenNextImage(0);
                }
            }
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (picMain.Image != null)
            {
                Graphics g = picMain.CreateGraphics();
                g.DrawImage(picMain.Image,
                            new Rectangle(0, 0, picMain.Right,
                                          picMain.Bottom - hScrollBar1.Height),
                            new Rectangle(hScrollBar1.Value, vScrollBar1.Value,
                                          picMain.Right - vScrollBar1.Width,
                                          picMain.Bottom - hScrollBar1.Height),
                            GraphicsUnit.Pixel);
                vScrollValue = vScrollBar1.Value;
                picMain.Update();
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {


                if (Directory.Exists(WorkingPath))
                {
                    MoveFilesFromWorkingFolderToSourceFolder();
                }
                else
                {
                    logger.Log(LogLevel.Info, "Directory does not exist :" + WorkingPath);
                    logger.Log(LogLevel.Info, "Could not move files from working folder to source folder ");
                }
                string remoteMachineName = ReadAppSettings("RemoteMachineName");
                if (!string.IsNullOrEmpty(remoteMachineName))
                {
                    // RemoteConnection.disconnectRemote(remoteMachineName);
                    // DriveSetting.DisconnectNetworkDrive("P", true); 
                }
                logger.Log(LogLevel.Info, "===SESSION CLOSING=====");
                logger.Log(LogLevel.Info, " Number of files to be processed at start of session: " + selectedFolderCount);
                logger.Log(LogLevel.Info, " Number of files PROCESSED at end of session : " + TotalFilesProcessed);
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Exception - " + ex.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + ex.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + ex.StackTrace);
            }

        }

        private void MoveFilesFromWorkingFolderToSourceFolder()
        {

            // string FileDir = ReadAppSettings(configDirectoryPath);
            string FileDir = selectedDirectoryLocation;
            if (Directory.Exists(WorkingPath))
            {

                string[] files = Directory.GetFiles(WorkingPath, "*.*", SearchOption.TopDirectoryOnly);

                logger.Log(LogLevel.Info, "No of files in working folder " + files.Length);
                foreach (string fileName in files)
                {
                    bool foundItem = false; 
                    foreach (FileDetail item in FileList)
                    {
                        if( item.workingFolderName == fileName || Path.GetFileName(item.imageFileName)== Path.GetFileName(fileName))
                        {
                            
                            if(!string.IsNullOrEmpty(item.sourceFileName))
                            {
                                foundItem = true;
                                string sourceFolderLocation = Path.GetDirectoryName(item.sourceFileName);
                                MoveFileFromWorkingFolderToSourceFolder(sourceFolderLocation, fileName);
                                break;
                            }
                        }
                    }


                    if(!foundItem)
                    MoveFileFromWorkingFolderToSourceFolder(FileDir, fileName);

                }
                logger.Log(LogLevel.Debug, "moved files from source to working folder");

                foreach (string dirName in Directory.GetDirectories(WorkingPath))
                {
                    logger.Log(LogLevel.Info, "Deleting the temp folders created:" + dirName);
                    Directory.Delete(dirName, true);
                }

            }

        }

        private static void MoveFileFromWorkingFolderToSourceFolder(string FileDir, string fileName)
        {
            try
            {


                string onlyFileName = Path.GetFileName(fileName);
                string sourceFile = Path.Combine(FileDir, onlyFileName);
                if (File.Exists(sourceFile))
                {
                    logger.Log(LogLevel.Info, " file found at source location: " + sourceFile);

                 //   File.Delete(fileName);
                }
                else
                {
                    logger.Log(LogLevel.Info, "Moving file " + fileName + " to location" + sourceFile);
                    File.Copy(fileName, sourceFile, true);
                    if(File.Exists(sourceFile))
                    {
                        logger.Log(LogLevel.Info, "File successfully copied at location:"+ sourceFile); 
                    }
                    else
                    {
                        logger.Log(LogLevel.Info, "File could not be moved :" + sourceFile);
                    }
                    logger.Log(LogLevel.Info, "Deleting file :" + fileName);
                    File.Delete(fileName);

                }
            }
            catch (Exception e)
            {

                logger.Log(LogLevel.Error, "MoveFileFromWOrkingFolderToSourceFolder" + e.StackTrace);
                logger.Log(LogLevel.Error, "Exception - " + e.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + e.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + e.StackTrace);
            }
        }

        private bool CheckIfPdfFileOrItsTextFile(string fileName)
        {

            string[] fileNames = Directory.GetFiles(selectedDirectoryLocation, Path.GetFileNameWithoutExtension(fileName) + ".*", SearchOption.AllDirectories);


            foreach (string pdfFileName in fileNames)
            {
                if (Path.GetExtension(pdfFileName) == ".pdf")
                {
                    return true;
                }
            }

            return false;
        }

        private void undoChangesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileFullName))
            {
                UndoChanges();
            }
        }

        private Image PictureBoxZoom(Image img, Size size)
        {
            Bitmap bm = new Bitmap(img, Convert.ToInt32(img.Width * size.Width), Convert.ToInt32(img.Height * size.Height));
            Graphics grap = Graphics.FromImage(bm);
            grap.InterpolationMode = InterpolationMode.HighQualityBicubic;
            return bm;
        }

        private void zoomSlider_Scroll(object sender, EventArgs e)
        {
            //if (zoomSlider.Value > 0)
            //{
            //    picMain.SizeMode = PictureBoxSizeMode.StretchImage;
            //    picMain.Image = null;
            //    Image imgOriginal = Image.FromFile(FileFullName);
            //    picMain.Image = PictureBoxZoom(imgOriginal, new Size(zoomSlider.Value, zoomSlider.Value));
            //    picMain.Refresh();
            //}
        }

        private void zoomSlider_ValueChanged(object sender, EventArgs e)
        {
            //if (zoomSlider.Value > 0)
            //    {
            //    //picMain.Image = null;
            //  //  picMain.Image = PictureBoxZoom(picMain.Image, new Size(zoomSlider.Value, zoomSlider.Value));
            //    }
        }

        private void btnEnableRedaction_Click(object sender, EventArgs e)
        {
            try
            {
                btnRedAct.Enabled = btnEnableRedaction.Enabled;
                btnDelete.Enabled = !btnRedAct.Enabled;
                if (btnEnableRedaction.Enabled && !Path.GetExtension(FileFullName).Contains("pdf"))
                {
                    picMain.SizeMode = PictureBoxSizeMode.Normal;
                    this.DisplayScrollBars();
                    this.SetScrollBarValues();
                    vScrollBar1.Height = picMain.Height;
                    hScrollBar1.Width = picMain.Width;
                    vScrollBar1.Value = 0;
                    hScrollBar1.Value = 0;
                    picMain.Refresh();
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Exception - " + ex.Message);
                logger.Log(LogLevel.Error, "Inner Exception- " + ex.InnerException);
                logger.Log(LogLevel.Error, "StackTrace - " + ex.StackTrace);
            }
        }
    } // end of class 
    public class FileDetail
    {
        /// <summary>
        /// 
        /// </summary>
        public string sourceFileName;
        public string workingFolderName;
        public string imageFileName;
        public string outputPathName;
        public FileDetail(string sourceName, string destinationName)
        {
            sourceFileName = sourceName;
            workingFolderName = destinationName;
        }
    }
}

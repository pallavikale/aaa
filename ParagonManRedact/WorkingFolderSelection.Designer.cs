﻿namespace ParagonManRedact
{
    partial class WorkingFolderSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkingFolderSelection));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNoOfHistoricDocuments = new System.Windows.Forms.Label();
            this.lblNoOfDailyDocuments = new System.Windows.Forms.Label();
            this.rbtnHistoric = new System.Windows.Forms.RadioButton();
            this.rbtnDaily = new System.Windows.Forms.RadioButton();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblNoOfHistoricDocuments);
            this.groupBox1.Controls.Add(this.lblNoOfDailyDocuments);
            this.groupBox1.Controls.Add(this.rbtnHistoric);
            this.groupBox1.Controls.Add(this.rbtnDaily);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(32, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 144);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select working directory";
            // 
            // lblNoOfHistoricDocuments
            // 
            this.lblNoOfHistoricDocuments.AutoSize = true;
            this.lblNoOfHistoricDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfHistoricDocuments.Location = new System.Drawing.Point(159, 85);
            this.lblNoOfHistoricDocuments.Name = "lblNoOfHistoricDocuments";
            this.lblNoOfHistoricDocuments.Size = new System.Drawing.Size(135, 15);
            this.lblNoOfHistoricDocuments.TabIndex = 3;
            this.lblNoOfHistoricDocuments.Text = "Number of documents: ";
            // 
            // lblNoOfDailyDocuments
            // 
            this.lblNoOfDailyDocuments.AutoSize = true;
            this.lblNoOfDailyDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfDailyDocuments.Location = new System.Drawing.Point(159, 46);
            this.lblNoOfDailyDocuments.Name = "lblNoOfDailyDocuments";
            this.lblNoOfDailyDocuments.Size = new System.Drawing.Size(132, 15);
            this.lblNoOfDailyDocuments.TabIndex = 2;
            this.lblNoOfDailyDocuments.Text = "Number of documents:";
            // 
            // rbtnHistoric
            // 
            this.rbtnHistoric.AutoSize = true;
            this.rbtnHistoric.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHistoric.Location = new System.Drawing.Point(19, 81);
            this.rbtnHistoric.Name = "rbtnHistoric";
            this.rbtnHistoric.Size = new System.Drawing.Size(66, 19);
            this.rbtnHistoric.TabIndex = 1;
            this.rbtnHistoric.TabStop = true;
            this.rbtnHistoric.Text = "Historic";
            this.rbtnHistoric.UseVisualStyleBackColor = true;
            // 
            // rbtnDaily
            // 
            this.rbtnDaily.AutoSize = true;
            this.rbtnDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnDaily.Location = new System.Drawing.Point(19, 42);
            this.rbtnDaily.Name = "rbtnDaily";
            this.rbtnDaily.Size = new System.Drawing.Size(52, 19);
            this.rbtnDaily.TabIndex = 0;
            this.rbtnDaily.TabStop = true;
            this.rbtnDaily.Text = "Daily";
            this.rbtnDaily.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(175, 216);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(107, 21);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Continue";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // WorkingFolderSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ParagonManRedact.Properties.Resources.grey;
            this.ClientSize = new System.Drawing.Size(494, 262);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WorkingFolderSelection";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paragon Manual Redaction Tool ";
            this.Activated += new System.EventHandler(this.WorkingFolderSelection_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WorkingFolderSelection_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WorkingFolderSelection_FormClosed);
            this.Load += new System.EventHandler(this.WorkingFolderSelection_Load);
            this.Shown += new System.EventHandler(this.WorkingFolderSelection_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnHistoric;
        private System.Windows.Forms.RadioButton rbtnDaily;
        private System.Windows.Forms.Button btnStart;
        public System.Windows.Forms.Label lblNoOfHistoricDocuments;
        public System.Windows.Forms.Label lblNoOfDailyDocuments;
    }
}
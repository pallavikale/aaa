﻿namespace ParagonManRedact
{
    
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineSplit = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redActToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnPrev = new System.Windows.Forms.ToolStripButton();
            this.firstPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.previousPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.nextPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.lastPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.btnNext = new System.Windows.Forms.ToolStripButton();
            this.btnRotate = new System.Windows.Forms.ToolStripButton();
            this.btnEnableRedaction = new System.Windows.Forms.ToolStripButton();
            this.btnRedAct = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.picMain = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fileStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.OpenFilesStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pageToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vScrollBar1.Location = new System.Drawing.Point(947, 67);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 84);
            this.vScrollBar1.TabIndex = 3;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineSplit});
            this.shapeContainer1.Size = new System.Drawing.Size(972, 588);
            this.shapeContainer1.TabIndex = 5;
            this.shapeContainer1.TabStop = false;
            // 
            // lineSplit
            // 
            this.lineSplit.Name = "lineSplit";
            this.lineSplit.X1 = 16;
            this.lineSplit.X2 = 817;
            this.lineSplit.Y1 = 465;
            this.lineSplit.Y2 = 465;
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hScrollBar1.Location = new System.Drawing.Point(0, 538);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(93, 17);
            this.hScrollBar1.TabIndex = 4;
            this.hScrollBar1.Visible = false;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(972, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previousToolStripMenuItem,
            this.nextToolStripMenuItem,
            this.rotateToolStripMenuItem,
            this.redActToolStripMenuItem,
            this.undoChangesToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.fileToolStripMenuItem.Text = "File Operations";
            // 
            // previousToolStripMenuItem
            // 
            this.previousToolStripMenuItem.Name = "previousToolStripMenuItem";
            this.previousToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.previousToolStripMenuItem.Text = "Open &Previous";
            this.previousToolStripMenuItem.Click += new System.EventHandler(this.previousToolStripMenuItem_Click);
            // 
            // nextToolStripMenuItem
            // 
            this.nextToolStripMenuItem.Name = "nextToolStripMenuItem";
            this.nextToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.nextToolStripMenuItem.Text = "Open &Next";
            this.nextToolStripMenuItem.Click += new System.EventHandler(this.nextToolStripMenuItem_Click);
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.rotateToolStripMenuItem.Text = "&Rotate";
            this.rotateToolStripMenuItem.Click += new System.EventHandler(this.rotateToolStripMenuItem_Click);
            // 
            // redActToolStripMenuItem
            // 
            this.redActToolStripMenuItem.Name = "redActToolStripMenuItem";
            this.redActToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.redActToolStripMenuItem.Text = "Red&act";
            this.redActToolStripMenuItem.Click += new System.EventHandler(this.redActToolStripMenuItem_Click);
            // 
            // undoChangesToolStripMenuItem
            // 
            this.undoChangesToolStripMenuItem.Name = "undoChangesToolStripMenuItem";
            this.undoChangesToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.undoChangesToolStripMenuItem.Text = "&Undo changes";
            this.undoChangesToolStripMenuItem.Click += new System.EventHandler(this.undoChangesToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.saveToolStripMenuItem.Text = "Release to &ImageRight with changes";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.deleteToolStripMenuItem.Text = "Release &without changes";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackgroundImage = global::ParagonManRedact.Properties.Resources.grey;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrev,
            this.firstPageToolStripButton,
            this.previousPageToolStripButton,
            this.nextPageToolStripButton,
            this.lastPageToolStripButton,
            this.btnNext,
            this.btnRotate,
            this.btnEnableRedaction,
            this.btnRedAct,
            this.btnUndo,
            this.btnSave,
            this.btnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(972, 40);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnPrev
            // 
            this.btnPrev.AutoSize = false;
            this.btnPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrev.Image = global::ParagonManRedact.Properties.Resources.ButtonPreviousIcon;
            this.btnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrev.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.btnPrev.Size = new System.Drawing.Size(35, 35);
            this.btnPrev.Text = "toolStripButton1";
            this.btnPrev.ToolTipText = "Open Previous File";
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // firstPageToolStripButton
            // 
            this.firstPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.firstPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("firstPageToolStripButton.Image")));
            this.firstPageToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.firstPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.firstPageToolStripButton.Name = "firstPageToolStripButton";
            this.firstPageToolStripButton.Size = new System.Drawing.Size(23, 37);
            this.firstPageToolStripButton.Text = "First";
            this.firstPageToolStripButton.Click += new System.EventHandler(this.firstPageToolStripButton_Click);
            // 
            // previousPageToolStripButton
            // 
            this.previousPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.previousPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("previousPageToolStripButton.Image")));
            this.previousPageToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.previousPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.previousPageToolStripButton.Name = "previousPageToolStripButton";
            this.previousPageToolStripButton.Size = new System.Drawing.Size(23, 37);
            this.previousPageToolStripButton.Text = "Previous";
            this.previousPageToolStripButton.Click += new System.EventHandler(this.previousPageToolStripButton_Click);
            // 
            // nextPageToolStripButton
            // 
            this.nextPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("nextPageToolStripButton.Image")));
            this.nextPageToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.nextPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextPageToolStripButton.Name = "nextPageToolStripButton";
            this.nextPageToolStripButton.Size = new System.Drawing.Size(23, 37);
            this.nextPageToolStripButton.Text = "Next";
            this.nextPageToolStripButton.Click += new System.EventHandler(this.nextPageToolStripButton_Click);
            // 
            // lastPageToolStripButton
            // 
            this.lastPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lastPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("lastPageToolStripButton.Image")));
            this.lastPageToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.lastPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.lastPageToolStripButton.Name = "lastPageToolStripButton";
            this.lastPageToolStripButton.Size = new System.Drawing.Size(23, 37);
            this.lastPageToolStripButton.Text = "Last";
            this.lastPageToolStripButton.Click += new System.EventHandler(this.lastPageToolStripButton_Click);
            // 
            // btnNext
            // 
            this.btnNext.AutoSize = false;
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNext.Image = global::ParagonManRedact.Properties.Resources.ButtonNextIcon;
            this.btnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(35, 35);
            this.btnNext.Text = "toolStripButton2";
            this.btnNext.ToolTipText = "Open Next File";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnRotate
            // 
            this.btnRotate.AutoSize = false;
            this.btnRotate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRotate.Image = global::ParagonManRedact.Properties.Resources.ActionsTransformRotateIcon;
            this.btnRotate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRotate.Margin = new System.Windows.Forms.Padding(0);
            this.btnRotate.Name = "btnRotate";
            this.btnRotate.Size = new System.Drawing.Size(35, 35);
            this.btnRotate.Text = "toolStripButton3";
            this.btnRotate.ToolTipText = "Rotate File";
            this.btnRotate.Click += new System.EventHandler(this.btnRotate_Click);
            // 
            // btnEnableRedaction
            // 
            this.btnEnableRedaction.AutoSize = false;
            this.btnEnableRedaction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnEnableRedaction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEnableRedaction.Image = global::ParagonManRedact.Properties.Resources.Checked_files_256;
            this.btnEnableRedaction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEnableRedaction.Name = "btnEnableRedaction";
            this.btnEnableRedaction.Size = new System.Drawing.Size(35, 35);
            this.btnEnableRedaction.Text = "toolStripButton1";
            this.btnEnableRedaction.ToolTipText = "Enable Image for Redaction";
            this.btnEnableRedaction.Click += new System.EventHandler(this.btnEnableRedaction_Click);
            // 
            // btnRedAct
            // 
            this.btnRedAct.AutoSize = false;
            this.btnRedAct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRedAct.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRedAct.Image = global::ParagonManRedact.Properties.Resources.RedActIcon;
            this.btnRedAct.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRedAct.Margin = new System.Windows.Forms.Padding(0);
            this.btnRedAct.Name = "btnRedAct";
            this.btnRedAct.Size = new System.Drawing.Size(35, 35);
            this.btnRedAct.Text = "Redact";
            this.btnRedAct.ToolTipText = "Redact File";
            this.btnRedAct.Click += new System.EventHandler(this.btnRedAct_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.AutoSize = false;
            this.btnUndo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::ParagonManRedact.Properties.Resources.UndoIcon;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Margin = new System.Windows.Forms.Padding(0);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(35, 35);
            this.btnUndo.ToolTipText = "Undo Actions";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = false;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = global::ParagonManRedact.Properties.Resources.ReleaseDoc;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(35, 35);
            this.btnSave.Text = "toolStripButton6";
            this.btnSave.ToolTipText = "Release to ImageRight with redaction changes";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSize = false;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::ParagonManRedact.Properties.Resources.ReleaseDocNoChange;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(35, 35);
            this.btnDelete.Text = "toolStripButton1";
            this.btnDelete.ToolTipText = "Release with no change";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // picMain
            // 
            this.picMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picMain.Location = new System.Drawing.Point(0, 67);
            this.picMain.Name = "picMain";
            this.picMain.Size = new System.Drawing.Size(945, 468);
            this.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMain.TabIndex = 0;
            this.picMain.TabStop = false;
            this.picMain.WaitOnLoad = true;
            this.picMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picMain_MouseDown);
            this.picMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picMain_MouseMove);
            this.picMain.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picMain_MouseUp);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileStatus,
            this.OpenFilesStatus,
            this.pageToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 566);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(972, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fileStatus
            // 
            this.fileStatus.Name = "fileStatus";
            this.fileStatus.Size = new System.Drawing.Size(107, 17);
            this.fileStatus.Text = "Processing file : 1 of ";
            // 
            // OpenFilesStatus
            // 
            this.OpenFilesStatus.Margin = new System.Windows.Forms.Padding(5, 3, 5, 2);
            this.OpenFilesStatus.Name = "OpenFilesStatus";
            this.OpenFilesStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OpenFilesStatus.Size = new System.Drawing.Size(840, 17);
            this.OpenFilesStatus.Spring = true;
            this.OpenFilesStatus.Text = "Files in working folder: ";
            this.OpenFilesStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pageToolStripStatusLabel
            // 
            this.pageToolStripStatusLabel.Name = "pageToolStripStatusLabel";
            this.pageToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(972, 588);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.picMain);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.shapeContainer1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(980, 615);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paragon Manual Redaction";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Resize += new System.EventHandler(this.Main_Resize_1);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picMain;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineSplit;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redActToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnPrev;
        private System.Windows.Forms.ToolStripButton btnNext;
        private System.Windows.Forms.ToolStripButton btnRotate;
        private System.Windows.Forms.ToolStripButton btnRedAct;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fileStatus;
        private System.Windows.Forms.ToolStripStatusLabel OpenFilesStatus;
        private System.Windows.Forms.ToolStripMenuItem undoChangesToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnEnableRedaction;
        private System.Windows.Forms.ToolStripButton firstPageToolStripButton;
        private System.Windows.Forms.ToolStripButton previousPageToolStripButton;
        private System.Windows.Forms.ToolStripButton nextPageToolStripButton;
        private System.Windows.Forms.ToolStripButton lastPageToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel pageToolStripStatusLabel;
    }


}


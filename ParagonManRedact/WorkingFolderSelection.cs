﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ParagonManRedact
{
    public partial class WorkingFolderSelection : Form
    {

        public string selectedFolder;
        public string dailyFolder;
        public string historicFolder;
        public int dailyFolderCount = 0;
        public int historicFolderCount = 0; 
        public bool isDailySelected = false;
        bool closeForm = false;
        public bool quitApplication = false; 
       
        public WorkingFolderSelection()
        {
            InitializeComponent();
        }

        public WorkingFolderSelection(string dailyFolderLocation, string historicFolderLocation)
        {
            InitializeComponent();
            dailyFolder = dailyFolderLocation;
            historicFolder = historicFolderLocation;
        }

     
        public void DisplayFileCountDetails()
        {
            //InitializeComponent();
            //dailyFolder = dailyFolderLocation;
            //historicFolder = historicFolderLocation;
            if (Directory.Exists(dailyFolder))
            {
                var files = Directory.EnumerateFiles(dailyFolder, "*.txt", SearchOption.AllDirectories);
                dailyFolderCount = files.ToList<string>().Count;
                lblNoOfDailyDocuments.Text += dailyFolderCount;
            }
            if (Directory.Exists(historicFolder))
            {
                var historicFiles = Directory.EnumerateFiles(historicFolder, "*.txt", SearchOption.AllDirectories);
                historicFolderCount = historicFiles.ToList<string>().Count;
                lblNoOfHistoricDocuments.Text += historicFolderCount;
            }
            if (dailyFolderCount == 0 && historicFolderCount == 0)
            {
                MessageBox.Show(Properties.Resources.NoFilesInSelectedFolder, Properties.Resources.Title, MessageBoxButtons.OK);
                quitApplication = true;
            }

        }

        // btn to continue to the next form 

        private void btnStart_Click(object sender, EventArgs e)
        {
            selectedFolder = (rbtnDaily.Checked == true) ? dailyFolder : historicFolder;
            isDailySelected = rbtnDaily.Checked;
            closeForm = true;
            this.Close(); 
        }

        private void WorkingFolderSelection_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void WorkingFolderSelection_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (closeForm == false)
            {

                DialogResult result = MessageBox.Show(Properties.Resources.QuitApplication, Properties.Resources.Title, MessageBoxButtons.YesNo);
               if (result == DialogResult.Yes)
               {
                   quitApplication = true;
               }
               else
               {
                   e.Cancel = true;
               }
            }
        }

        private void WorkingFolderSelection_Shown(object sender, EventArgs e)
        {
            
            DisplayFileCountDetails();
        }

        private void WorkingFolderSelection_Load(object sender, EventArgs e)
        {
           
        }

        private void WorkingFolderSelection_Activated(object sender, EventArgs e)
        {
           // DisplayFileCountDetails();
        }




    }
}
